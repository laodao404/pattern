package com.xx3.pattern.state.test;

import com.xx3.pattern.state.client.GumballMachine;
import org.junit.Test;

/**
 * Created by XiongZhengHai on 2017/11/27.
 */
public class Test1 {

    @Test
    public void test(){

        GumballMachine gumballMachine = new GumballMachine(2);

        gumballMachine.insertQuarter();
        gumballMachine.turnCrack();
        gumballMachine.ejectQuarter();

    }
}
