package com.xx3.pattern.state.client;

import com.xx3.pattern.state.base.*;

/**
 * Created by XiongZhengHai on 2017/11/27.
 */
public class GumballMachine {

    private SoldState soldState;
    private SoldOutState soldOutState;
    private HasQuartterState hasQuartterState;
    private NoQuarterState noQuarterState;

    private State state;
    private int count = 0;

    // 投入硬币
    public void insertQuarter(){
        state.insertQuarter();
    }

    // 退出硬币
    public void ejectQuarter(){
        state.ejectQuarter();
    }

    // 转动手柄
    public void turnCrack(){
        state.turnCrack();
        state.dispense();
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("\nMighty Gumball, Inc.");
        result.append("\nJava-enabled Standing Gumball Model #2004");
        result.append("\nInventory: " + count + " gumball");
        if (count != 1) {
            result.append("s");
        }
        result.append("\n");
        result.append("Machine is " + state + "\n");
        return result.toString();
    }

    public int getCount() {
        return count;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public SoldState getSoldState() {
        return soldState;
    }

    public void setSoldState(SoldState soldState) {
        this.soldState = soldState;
    }

    public SoldOutState getSoldOutState() {
        return soldOutState;
    }

    public void setSoldOutState(SoldOutState soldOutState) {
        this.soldOutState = soldOutState;
    }

    public HasQuartterState getHasQuartterState() {
        return hasQuartterState;
    }

    public void setHasQuartterState(HasQuartterState hasQuartterState) {
        this.hasQuartterState = hasQuartterState;
    }

    public NoQuarterState getNoQuarterState() {
        return noQuarterState;
    }

    public void setNoQuarterState(NoQuarterState noQuarterState) {
        this.noQuarterState = noQuarterState;
    }

    public void releaseBall(){
        System.out.println(" 您获得一颗糖果. ");
        if(count != 0){
            count -= 1;
        }

    }



    public GumballMachine(int count) {
        soldState = new SoldState(this);
        soldOutState = new SoldOutState(this);
        hasQuartterState = new HasQuartterState(this);
        noQuarterState = new NoQuarterState(this);
        this.count = count;
        if(count > 0){
            state = noQuarterState;
        }
    }
}
