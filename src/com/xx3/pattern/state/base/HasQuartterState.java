package com.xx3.pattern.state.base;

import com.xx3.pattern.state.client.GumballMachine;

/**
 * Created by XiongZhengHai on 2017/11/27.
 */
public class HasQuartterState implements State {

    private GumballMachine gumballMachine;

    @Override
    public void insertQuarter() {
        System.out.println(" 您已经投过币. ");
    }

    @Override
    public void ejectQuarter() {
        System.out.println(" 硬币正在退回. ");
        gumballMachine.setState(gumballMachine.getNoQuarterState());
    }

    @Override
    public void turnCrack() {
        System.out.println(" 您转动了手柄. ");
        gumballMachine.setState(gumballMachine.getSoldState());
    }

    @Override
    public void dispense() {
        System.out.print(" 没有糖果要分发. ");
    }

    public HasQuartterState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }
}
