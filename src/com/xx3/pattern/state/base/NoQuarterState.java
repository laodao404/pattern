package com.xx3.pattern.state.base;

import com.xx3.pattern.state.client.GumballMachine;


/**
 * Created by XiongZhengHai on 2017/11/27.
 */
public class NoQuarterState implements State {
    private GumballMachine gumballMachine;

    @Override
    public void insertQuarter() {
        System.out.println(" 您插入一个硬币.");
        this.gumballMachine.setState(gumballMachine.getHasQuartterState());
    }

    @Override
    public void ejectQuarter() {
        System.out.println(" 您还没有投过硬币. ");
    }

    @Override
    public void turnCrack() {
        System.out.println(" 请先投币,再转动手柄. ");
    }

    @Override
    public void dispense() {
        System.out.println(" 请先投币. ");
    }

    public NoQuarterState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }
}
