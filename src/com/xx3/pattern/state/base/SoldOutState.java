package com.xx3.pattern.state.base;

import com.xx3.pattern.state.client.GumballMachine;

/**
 * Created by XiongZhengHai on 2017/11/27.
 */
public class SoldOutState implements State {
    private GumballMachine gumballMachine;

    @Override
    public void insertQuarter() {
        System.out.println(" 糖果已经售空. ");
    }

    @Override
    public void ejectQuarter() {
        System.out.println(" 您还没有投过硬币. ");
    }

    @Override
    public void turnCrack() {
        System.out.println(" 请先投币,再转动手柄. ");
    }

    @Override
    public void dispense() {
        System.out.println(" 请先投币. ");
    }

    public SoldOutState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }
}
