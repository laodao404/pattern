package com.xx3.pattern.state.base;

import com.xx3.pattern.state.client.GumballMachine;

/**
 * Created by XiongZhengHai on 2017/11/27.
 */
public class SoldState implements State {
    private GumballMachine gumballMachine;

    @Override
    public void insertQuarter() {
        System.out.println(" 请销待,正在分发糖果. ");
    }

    @Override
    public void ejectQuarter() {
        System.out.println(" 对不起,您已经转动过手柄. ");

    }

    @Override
    public void turnCrack() {
        System.out.println(" 一次投币, 只能分发一颗糖果. ");
    }

    @Override
    public void dispense() {
        gumballMachine.releaseBall();
        if(gumballMachine.getCount() > 0){
            gumballMachine.setState(gumballMachine.getNoQuarterState());
        }else{
            gumballMachine.setState(gumballMachine.getSoldOutState());
        }
    }

    public SoldState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }
}
