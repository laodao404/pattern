package com.xx3.pattern.state.base;

/**
 * Created by XiongZhengHai on 2017/11/27.
 */
public interface State {
    // 投入硬币
    void insertQuarter();
    // 退出硬币
    void ejectQuarter();
    // 转动手柄
    void turnCrack();
    // 分发糖果
    void dispense();
}
