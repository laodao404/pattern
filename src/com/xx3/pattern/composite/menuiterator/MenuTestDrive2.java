package com.xx3.pattern.composite.menuiterator;

import org.junit.Test;

public class MenuTestDrive2 {



	@Test
	public void test01 (){


		MenuComponent dinerMenu =
			new Menu("正餐菜单", "Lunch");

		MenuComponent dessertMenu =
			new Menu("甜点菜单", "Dessert of course!");

		MenuComponent allMenus = new Menu("ALL MENUS", "All menus combined");
  
		allMenus.add(dinerMenu);
		dinerMenu.add(dessertMenu);

//		dinerMenu.add(new MenuItem(
//			"素食饼",
//			"(Fakin') Bacon with lettuce & tomato on whole wheat",
//			true,
//			2.99));


		dessertMenu.add(new MenuItem(
			"苹果派",
			"Apple pie with a flakey crust, topped with vanilla icecream",
			true,
			1.59));
//		dessertMenu.add(new MenuItem(
//			"芝士蛋糕",
//			"Creamy New York cheesecake, with a chocolate graham crust",
//			true,
//			1.99));


		Waitress waitress = new Waitress(allMenus);
   
//		waitress.printVegetarianMenu();
		waitress.printMenu();
 
	}
}
